package añosbisiestos;

/**
 *
 * @author Josue Daniel Roldan Ochoa.
 */
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
public class AñosBisiestos {
public static void main(String[] args) throws IOException {
BufferedReader rd=new BufferedReader(new InputStreamReader(System.in));
System.out.println("Escriba el Año");
String respuesta=rd.readLine();
Integer rsp=Integer.parseInt(respuesta);
System.out.println(es_bisiesto(rsp));

}
static boolean es_bisiesto(int anio){
boolean resultado=false;
if(anio%4==0){
if(anio%100==0){
if(anio%400==0){
resultado=true;
}else{
resultado=false;
}
}else{
resultado=true;
}
}else{ 
resultado=false;
}
return resultado;
}
    
}
